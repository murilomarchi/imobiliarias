<?php
if(!isset($_POST['operation'])){
    echo json_encode(array('status'=>'ERROR', 'reason'=>'MISSING_PARAMS'));
    exit;
}

session_start();
if($_POST['operation'] == 'open'){
    if(!isset($_POST['screen_opened'])){
        echo json_encode(array('status'=>'ERROR', 'reason'=>'MISSING_PARAMS'));
        exit;
    }
    switch ($_POST['screen_opened']){    
        case 'contratos':
            $_SESSION['screen_opened'] = 'contratos';
        break;
// ----------------------------------------------------------------------------------------------------
        case 'pessoas':
            $_SESSION['screen_opened'] = 'pessoas';
        break;
// ----------------------------------------------------------------------------------------------------
        case 'imoveis':
            $_SESSION['screen_opened'] = 'imoveis';
        break;
// ----------------------------------------------------------------------------------------------------
    case 'novo_contrato':
        $_SESSION['screen_opened'] = 'novo_contrato';
    break;
// ----------------------------------------------------------------------------------------------------
    case 'nova_despesa':
        $_SESSION['screen_opened'] = 'nova_despesa';
    break;
// ----------------------------------------------------------------------------------------------------
    case 'novo_p':
        $_SESSION['screen_opened'] = 'novo_p';
    break;
// ----------------------------------------------------------------------------------------------------
    case 'consultar':
        $_SESSION['screen_opened'] = 'consultar';
    break;
// ----------------------------------------------------------------------------------------------------
    case 'novo_i':
        $_SESSION['screen_opened'] = 'novo_i';
    break;
// ----------------------------------------------------------------------------------------------------

    }
}elseif($_POST['operation'] == 'back'){
    if(!isset($_POST['screen'])){
        echo json_encode(array('status'=>'ERROR', 'reason'=>'MISSING_PARAMS'));
        exit;
    }
    switch ($_POST['screen']){    
        case 'main_menu':
            session_destroy();
        break;
    // ----------------------------------------------------------------------------------------------------
    }
}

echo json_encode(array('status'=>'OK', 'debug' => $_SESSION['screen_opened']));
exit;

?>