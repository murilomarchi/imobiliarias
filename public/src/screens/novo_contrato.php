<?php
    require_once dirname(__FILE__) . '/header.php';
?>
<h1 class="new-header">Novo Contrato<h1>
<div class="new-container">  
    <form id="new_form" onsubmit="mainjs.saveForm(event, 'novo_contrato')">
        <div>  
            <input type="text" name="INQUILINOS[0][ID_PESSOA_PES]" placeholder="ID gerado no cadastro do locatário"/>
            <input type="text" name="INQUILINOS[0][FL_PRINCIPAL_INQ]" placeholder="Caso o contrato tenha mais de 1 locatário esse campo é OBRIGATÓRIO"/>
            <input type="text" name="INQUILINOS[0][NM_FRACAO_INQ]" placeholder="Percentual no recibo "/>
            <input type="text" name="ID_IMOVEL_IMO" placeholder="ID do imóvel"/>
            <input type="text" name="ID_TIPO_CON" placeholder="Tipo de contrato"/>
            <input type="text" name="DT_INICIO_CON" placeholder="Data de inicio do contrato"/>
            <input type="text" name="DT_FIM_CON" placeholder="Data de termino do contrato"/>
            <input type="text" name="VL_ALUGUEL_CON" placeholder="Valor do aluguel"/>
            <input type="text" name="TX_ADM_CON" placeholder="Taxa de administração"/>
            <input type="text" name="FL_TXADMVALORFIXO_CON" placeholder="Flag para definir um valor fixo para taxa, ao invés de percentual"/>
            <input type="text" name="NM_DIAVENCIMENTO_CON" placeholder="Dia de vencimento do contrato"/>
            <input type="text" name="TX_LOCACAO_CON" placeholder="Valor da taxa de locação"/>
            <input type="text" name="ID_INDICEREAJUSTE_CON" placeholder="ID do indice de reajuste"/>
            <input type="text" name="NM_MESREAJUSTE_CON" placeholder="Mês de reajuste do contrato"/>
            <input type="text" name="DT_ULTIMOREAJUSTE_CON" placeholder="Data do ultimo reajuste "/>
            <input type="text" name="FL_MESFECHADO_CON" placeholder="Flag de periodo do aluguel com mês fechado"/>
            <input type="text" name="ID_CONTABANCO_CB" placeholder="ID do banco cadastrado na licença"/>
            <input type="text" name="FL_DIAFIXOREPASSE_CON" placeholder="Flag para o dia do repasse"/>
            <input type="text" name="NM_DIAREPASSE_CON" placeholder="Numero de dias para repasse"/>
            <input type="text" name="FL_MESVENCIDO_CON" placeholder="Forma de cobrança"/>
            <input type="text" name="FL_DIMOB_CON" placeholder="Gerar DIMOB pelo sistema"/>
            <input type="text" name="ID_FILIAL_FIL" placeholder="ID da filial cadastrada no sistema "/>
            <input type="text" name="ST_OBSERVACAO_CON" placeholder="Observações do contrato"/>
            <input type="text" name="NM_REPASSEGARANTIDO_CON" placeholder="Repasse garantido ou numero de meses garantidos"/>
            <input type="text" name="FL_GARANTIA_CON" placeholder="garantia"/>
            <input type="text" name="FL_SEGUROINCENDIO_CON" placeholder="seguro"/>
            <input type="text" name="FL_ENDCOBRANCA_CON" placeholder="Endereço de cobrança"/>
            <button type="submit">Salvar</button>   
        </div>
    </form>
</div>