<?php
session_start();
if(isset($_SESSION['screen_opened'])){
    switch ($_SESSION['screen_opened']){    
        case 'pessoas':
            require_once dirname(__FILE__) . '/menu_pessoas.php';
        break;
// ----------------------------------------------------------------------------------------------------
        case 'contratos':
            require_once dirname(__FILE__) . '/menu_contratos.php';
        break;
// ----------------------------------------------------------------------------------------------------
        case 'imoveis':
            require_once dirname(__FILE__) . '/menu_imoveis.php';
        break;
// ----------------------------------------------------------------------------------------------------
        case 'novo_contrato':
            require_once dirname(__FILE__) . '/novo_contrato.php';
        break;
// ----------------------------------------------------------------------------------------------------
        case 'nova_despesa':
            require_once dirname(__FILE__) . '/nova_despesa.php';
        break;
// ----------------------------------------------------------------------------------------------------
        case 'novo_p':
            require_once dirname(__FILE__) . '/novo_p.php';
        break;
// ----------------------------------------------------------------------------------------------------
        case 'consultar':
            require_once dirname(__FILE__) . '/consultar.php';
        break;
// ----------------------------------------------------------------------------------------------------
        case 'novo_i':
            require_once dirname(__FILE__) . '/novo_i.php';
        break;
// ----------------------------------------------------------------------------------------------------

    }
}else{
    require_once dirname(__FILE__) . '/main.php';
}
?>