<?php
    require_once dirname(__FILE__) . '/header.php';
?>
<h1 class="new-header">Cadastro de Pessoas<h1>
<div class="new-container">  
    <form id="new_form" onsubmit="mainjs.saveForm(event, 'novo_imovel')">
        <div>  
            <?if(isset($resultado)){?>
                <input type="hidden" id="id_pessoa_pes" name="id_pessoa_pes" value="<?echo $resultado->data[0]->id_pessoa_pes?>" />
            <?}?>
            <input type="text" name="tipo" placeholder="proprietarios, locatarios, fiadores, corretores"/>
            <input type="text" <?echo ( isset($resultado) && isset($resultado->data[0]->st_nome_pes ) ) ? ' value="'. $resultado->data[0]->st_nome_pes .'"' : '' ?> name="st_nome_pes" placeholder="Nome"/>
            <input type="text" <?echo ( isset($resultado) && isset($resultado->data[0]->st_fantasia_pes ) ) ? ' value="'. $resultado->data[0]->st_fantasia_pes .'"' : '' ?> name="st_fantasia_pes" placeholder="Nome Fantasia"/>
            <input type="text" <?echo ( isset($resultado) && isset($resultado->data[0]->st_cnpj_pes ) ) ? ' value="'. $resultado->data[0]->st_cnpj_pes .'"' : '' ?> name="st_cnpj_pes" placeholder="CPF ou CNPJ"/>
            <input type="text" <?echo ( isset($resultado) && isset($resultado->data[0]->st_celular ) ) ? ' value="'. $resultado->data[0]->st_celular .'"' : '' ?> name="st_celular" placeholder="Celular"/>
            <input type="text" <?echo ( isset($resultado) && isset($resultado->data[0]->st_telefone_pes ) ) ? ' value="'. $resultado->data[0]->st_telefone_pes .'"' : '' ?> name="st_telefone_pes" placeholder="Telefone"/>
            <input type="text" <?echo ( isset($resultado) && isset($resultado->data[0]->st_email_pes ) ) ? ' value="'. $resultado->data[0]->st_email_pes .'"' : '' ?> name="st_email_pes" placeholder="Email"/>
            <input type="text" <?echo ( isset($resultado) && isset($resultado->data[0]->st_rg_pes ) ) ? ' value="'. $resultado->data[0]->st_rg_pes .'"' : '' ?> name="st_rg_pes" placeholder="RG"/>
            <input type="text" <?echo ( isset($resultado) && isset($resultado->data[0]->st_orgao_pes ) ) ? ' value="'. $resultado->data[0]->st_orgao_pes .'"' : '' ?> name="st_orgao_pes" placeholder="Orgão Exp."/>
            <input type="text" <?echo ( isset($resultado) && isset($resultado->data[0]->st_sexo_pes ) ) ? ' value="'. $resultado->data[0]->st_sexo_pes .'"' : '' ?> name="st_sexo_pes" placeholder="Sexo"/>
            <input type="text" <?echo ( isset($resultado) && isset($resultado->data[0]->dt_nascimento_pes ) ) ? ' value="'. $resultado->data[0]->dt_nascimento_pes .'"' : '' ?> name="dt_nascimento_pes" placeholder="Dt Nascimento"/>
            <input type="text" <?echo ( isset($resultado) && isset($resultado->data[0]->st_nacionalidade_pes ) ) ? ' value="'. $resultado->data[0]->st_nacionalidade_pes .'"' : '' ?> name="st_nacionalidade_pes" placeholder="Nacionalidade"/>
            <input type="text" <?echo ( isset($resultado) && isset($resultado->data[0]->st_cep_pes ) ) ? ' value="'. $resultado->data[0]->st_cep_pes .'"' : '' ?> name="st_cep_pes" placeholder="CEP"/>
            <input type="text" <?echo ( isset($resultado) && isset($resultado->data[0]->st_endereco_pes ) ) ? ' value="'. $resultado->data[0]->st_endereco_pes .'"' : '' ?> name="st_endereco_pes" placeholder="Endereco"/>
            <input type="text" <?echo ( isset($resultado) && isset($resultado->data[0]->st_numero_pes ) ) ? ' value="'. $resultado->data[0]->st_numero_pes .'"' : '' ?> name="st_numero_pes" placeholder="Numero"/>
            <input type="text" <?echo ( isset($resultado) && isset($resultado->data[0]->st_complemento_pes ) ) ? ' value="'. $resultado->data[0]->st_complemento_pes .'"' : '' ?> name="st_complemento_pes" placeholder="Complemento"/>
            <input type="text" <?echo ( isset($resultado) && isset($resultado->data[0]->st_bairro_pes ) ) ? ' value="'.$resultado->data[0]->st_bairro_pes .'"' : '' ?> name="st_bairro_pes" placeholder="Bairro"/>
            <input type="text" <?echo ( isset($resultado) && isset($resultado->data[0]->st_cidade_pes ) ) ? ' value="'. $resultado->data[0]->st_cidade_pes .'"' : '' ?> name="st_cidade_pes" placeholder="Cidade"/>
            <input type="text" <?echo ( isset($resultado) && isset($resultado->data[0]->st_estado_pes ) ) ? ' value="'. $resultado->data[0]->st_estado_pes .'"' : '' ?> name="st_estado_pes" placeholder="Estado"/>
            <input type="text" <?echo ( isset($resultado) && isset($resultado->data[0]->st_observacao_pes ) ) ? ' value="'. $resultado->data[0]->st_observacao_pes .'"' : '' ?> name="st_observacao_pes" placeholder="Obs"/>

            <button type="submit">Salvar</button>     
        </div>
    </form>
</div>