<?php
    require_once dirname(__FILE__) . '/header.php';
?>
<h1 class="new-header">Nova Despesa<h1>
<div class="new-container">  
    <form id="new_form" onsubmit="mainjs.saveForm(event, 'nova_despesa')">
            <div>  
            <input type="text" name="ID_IMOVEL_IMO" placeholder="ID do imóvel"/>
            <input type="text" name="ID_CONTRATO_CON" placeholder="ID do contrato"/>
            <input type="text" name="DT_LANCAMENTO_IMOD" placeholder="Data de vencimento da despesa"/>
            <input type="text" name="VALOR" placeholder="Valor da despesa"/>
            <input type="text" name="ID_PRODUTO_PRD" placeholder="ID do produto"/>
            <input type="text" name="ST_COMPLEMENTO" placeholder="Complemento da despesa"/>
            <input type="text" name="ID_DEBITO_IMOD" placeholder="Debito da despesa"/>
            <input type="text" name="ID_CREDITO_IMOD" placeholder="Credito da despesa"/>
            <input type="text" name="ID_FORMAPAGAMENTO_IMOD" placeholder="Forma de pagamento da despesa"/>
            <input type="text" name="NM_PARCELAS" placeholder="Numero da parcela"/>
            <input type="text" name="NM_PARCELAS_FIM" placeholder="Total de parcelas"/>
            <input type="text" name="FL_COBRARTXADM_IMOD" placeholder="Flag para calcular taxa de administração sobre o serviço da despesa"/>
            <input type="text" name="ID_CONTABANCO_CB" placeholder="ID do banco cadastrado na licença"/>
            <input type="text" name="ST_CODIGOBARRAS_MOV" placeholder="Codigo de barras"/>
        
            <button type="submit">Salvar</button>   
        </div>
    </form>
</div>