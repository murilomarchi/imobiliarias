<?php
    require_once dirname(__FILE__) . '/header.php';
?>
<h1 class="new-header">Cadastro de Imóveis<h1>
<div class="new-container">  
    <form id="new_form" onsubmit="mainjs.saveForm(event, 'novo_imovel')">
        <div>  
        
            <input type="text" name="PROPRIETARIOS_BENEFICIARIOS[0][ID_PESSOA_PES]" placeholder="Proprietario"/>
            <input type="text" name="PROPRIETARIOS_BENEFICIARIOS[0][FL_PROPRIETARIO_PRB]" placeholder="Serve para identificar o proprietário principal do imóvel e os beneficiarios:"/>
            <input type="text" name="PROPRIETARIOS_BENEFICIARIOS[0][NM_FRACAO_PRB]" placeholder="Serve para identificar o percentual de repasse dos proprietários"/>
            <input type="text" name="ST_TIPO_IMO" placeholder="Tipo de imóvel "/>
            <input type="text" name="ST_CEP_IMO" placeholder="CEP"/>
            <input type="text" name="ST_ENDERECO_IMO" placeholder="Endereço"/>
            <input type="text" name="ST_NUMERO_IMO" placeholder="Número"/>
            <input type="text" name="ST_COMPLEMENTO_IMO" placeholder="Comlemento"/>
            <input type="text" name="ST_BAIRRO_IMO" placeholder="Bairro"/>
            <input type="text" name="ST_CIDADE_IMO" placeholder="Cidade"/>
            <input type="text" name="ST_ESTADO_IMO" placeholder="Estado"/>
            <input type="text" name="ST_IDENTIFICADOR_IMO" placeholder="Identificador do imóvel em outro sistema"/>
            <input type="text" name="VL_ALUGUEL_IMO" placeholder="Valor do aluguel"/>
            <input type="text" name="VL_VENDA_IMO" placeholder="Valor de venda"/>
            <input type="text" name="TX_ADM_IMO" placeholder="Taxa de administração mensal "/>
            <input type="text" name="FL_TXADMVALORFIXO_IMO" placeholder="Boolean"/>


            <button type="submit">Salvar</button>     
        </div>
    </form>
</div>