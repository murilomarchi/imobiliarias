<?php
class Request{
    public  $url           = null;
    public  $server_output = null;
    public  $postfields    = array();
    public  $headers       = array();
    
    function __construct($username = null){
        $this->headers = array(
                                'Content-Type: application/json', 
                                'app_token:'.Tokens::app_token,
                                'access_token:'.Tokens::access_token 
                        );
    }  

    function sendPost(){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->postfields));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $this->server_output = curl_exec($ch);
        curl_close($ch);
    }

    function senGet(){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $this->server_output = curl_exec($ch);
        curl_close($ch);
    }

    function sendPut(){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->postfields));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,  'PUT');

        $this->server_output = curl_exec($ch);
        curl_close($ch);
    }
}
    
