var mainjs = {
    closeModal: function() {
        $('#modal').css('display', 'none');
    },
    openModal: function(code){
        var text = code;
        if(code == 'MISSING_PARAMS'){
            text = 'Missing Required Parameters';
        }else if(code == 'INVALID_USERNAME'){
            text = 'Please insert a valid Username';
        }else if(code == 'new_user'){
            text = 'User Not found, Redirecting to Sing Up area ';
        }else if(code == 'LOGIN_ERROR'){
            text = 'Internal Error';
        }else if(code == 'UNABLE_TO_CREATE_USER'){
            text = 'Unable to create a new user';
        }else if(code == 'CREATE_ERROR'){
            text = 'Error while creating a new user';
        }else if(code == 'USER_CREATED'){
            text = 'User Created, logging in ...'
        }else if(code == 'USER_DELETED'){
            text = 'User Deleted'
        }

        $('#modal_content').html(text);
        $('#modal').css('display', 'block');
    },
    openScreen: function(e, obj) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "./src/screens/screen_navigation.php",
            data: {operation: 'open', screen_opened: $(obj).val()},
            dataType:'JSON', 
            success: function(response){
                if(response.status == 'OK'){
                    location.href='./';
                }else{
                    mainjs.openModal(response.reason);
                }
            },
        });
    },
    backScreen: function(e, screen){
        $.ajax({
            type: "POST",
            url: "./src/screens/screen_navigation.php",
            data: {operation: 'back', screen: screen},
            dataType:'JSON', 
            success: function(response){
                location.href = './';
            }
        }); 
    },
    saveForm: function(e, operation){
        e.preventDefault();
        var fields = {};
        var a = $("#new_form").serializeArray();
        $.each(a, function() {
            if (fields[this.name]) {
                if (!fields[this.name].push) {
                    fields[this.name] = [fields[this.name]];
                }
                fields[this.name].push(this.value || '');
            } else {
                fields[this.name] = this.value || '';
            }
        });
        $.ajax({
            type: "POST",
            url: "./src/controllers/homecontroller.php",
            data: {operation: operation, fields: fields},
            dataType:'JSON', 
            success: function(response){
                mainjs.openModal(response.msg);
            },
        });
    }
}