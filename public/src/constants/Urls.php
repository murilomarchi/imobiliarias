<?php

abstract class Urls {
    const url = "https://apps.superlogica.net/imobiliaria/api/";
    const contratos = "contratos";
    const despesas = "imoveisdespesa";
    const imoveis = "imoveis";
    const proprietarios = "proprietarios";
    const locatarios = "locatarios";
    const fiadores = "fiadores";
    const corretores = "corretores";

}