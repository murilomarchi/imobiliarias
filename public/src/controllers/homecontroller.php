<?php
include_once dirname(__FILE__) . "/../config/autoload.php";
    
if(!isset($_POST['operation'])){
    echo json_encode(array('status'=>'ERROR', 'reason'=>'MISSING_PARAMS'));
    exit;
}
$request = new Request();
$request->url = Urls::url;
switch ($_POST['operation']){

    case 'consultar':
        switch($_GET['tipo']){
            case 'proprietarios': $request->url .= Urls::proprietarios; ;break;
            case 'locatarios': $request->url .= Urls::locatarios; ;break;
            case 'fiadores': $request->url .= Urls::fiadores; ;break;
            case 'corretores': $request->url .= Urls::corretores; ;break;
        }
        if(isset($_GET['id'])){
            $request->url .= '?id='.$_GET['id'];       
        }else{
            $request->url .= '?pagina='.$page;       
        }
        
        $request->senGet();

        $resultado = json_decode($request->server_output);
    break;

// ----------------------------------------------------------------------------------------------------
    case 'novo_contrato':
        $request->url .= Urls::contratos;
        $request->postfields = $_POST['fields'];
        $request->sendPost();

        echo $request->server_output;
    break;
// ----------------------------------------------------------------------------------------------------
    case 'nova_despesa':
        $request->url .= Urls::despesas;
        $request->postfields = $_POST['fields'];
        $request->sendPost();

        echo $request->server_output;
    break;
// ----------------------------------------------------------------------------------------------------
    case 'novo_imovel':
        $request->url .= Urls::imoveis;
        $request->postfields = $_POST['fields'];
        $request->sendPost();

        echo $request->server_output;
    break;  
// ----------------------------------------------------------------------------------------------------
    case 'nova_pessoa':
        switch($_POST['fields']['tipo']){
            case 'proprietarios': $request->url .= Urls::proprietarios; ;break;
            case 'locatarios': $request->url .= Urls::locatarios; ;break;
            case 'fiadores': $request->url .= Urls::fiadores; ;break;
            case 'corretores': $request->url .= Urls::corretores; ;break;
        }
        
        $request->postfields = $_POST['fields'];
        $request->sendPost();

        echo $request->server_output;
    break;  
}

