<?
//session_start();
//session_destroy();
?>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="./src/js/main.js"></script>
    <link rel="stylesheet" type="text/css" href="./src/css/main.css">

    <!-- Modal Code -->
    <div id="modal" class="modal">
        <div class="modal-content">
            <span class="close" onclick="mainjs.closeModal()">&times;</span>
            <p id="modal_content"></p>
            <button onclick="mainjs.backScreen(event, 'main_menu')">OK</button>
        </div>
    </div>    
    <!-- End Modal Code -->

    <title>Murilo Imobiliária</title>
  </head>
  <body>
    <?php require_once dirname(__FILE__) . '/src/screens/screens.php';//handle content of the page?>
  </body>
</html>